aspell-or (0.03-1-7) unstable; urgency=low

  [ Kartik Mistry ]
  * Team upload.
  * Added debian/gitlab-ci.yml
  * debian/control:
    + Updated Standards-Version to 4.5.0
    + Added 'Rules-Requires-Root' field.
    + Switched to debhelper-compat.

  [ Christopher Hoskin ]
  * debian/control:
   + Use new team e-mail address in control and copyright.
   + Move from Section text to localization.

 -- Kartik Mistry <kartik@debian.org>  Sun, 12 Apr 2020 09:30:50 +0530

aspell-or (0.03-1-6) unstable; urgency=medium

  * Team upload.
  * Bump compat from 5 to 11
  * Update VCS to Salsa
  * Use secure copyright format uri
  * Update debian/copyright
  * Use secure URI for debian/watch
  * Bump Standards-Version from 3.9.3 to 4.3.0.2 (no change required)
  * Add Autopkgtest test
  * Use dh_aspell-simple
  * Add myself to Uploaders

 -- Christopher Hoskin <mans0954@debian.org>  Sun, 10 Feb 2019 21:58:34 +0000

aspell-or (0.03-1-5) unstable; urgency=low

  [Kartik Mistry]
  * Team upload.
  * debian/control:
    + Updated Vcs-* fields.
    + Bumped Standards-Version to 3.9.3
  * debian/copyright:
    + Updated to copyright-format 1.0

 -- Kartik Mistry <kartik@debian.org>  Wed, 14 Mar 2012 20:32:26 +0530

aspell-or (0.03-1-4) unstable; urgency=low

  [Vasudev Kamath]
  * debian/copyright:
    + Fixed dep5 Format URL.
    + Created a proper License block.
  * debian/rules:
    + Excluded var/lib/aspell from md5sum calculation. (Closes: #638732)

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sat, 05 Nov 2011 17:34:19 +0530

aspell-or (0.03-1-3) unstable; urgency=low

  [Vasudev Kamath]
  * debian/control:
    + Bumped Standards-Version to 3.9.2
    + Removed cdbs dependency
  * debian/copyright:
    + Updated to latest DEP-5 specifications
    + Updated copyright for Debian packaging
  * debian/rules:
    + Changed from cdbs to dh7

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sat, 04 Jun 2011 19:34:16 +0530

aspell-or (0.03-1-2) unstable; urgency=low

  [Kartik Mistry]
  * debian/source/format:
    + Updated package to use source format 3.0 (quilt)
  * debian/control:
    + Updated Standards-Version to 3.8.4 (no changes needed)
    + Wrapped up Build-Depends and Depends
  * debian/copyright:
    + Updated as per DEP-5 specifications

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Wed, 12 May 2010 10:09:31 +0530

aspell-or (0.03-1-1) unstable; urgency=low

  [Kartik Mistry]
  * New upstream release
  * debian/copyright:
    + Don't use versionless symlink for licenses

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Tue, 04 Aug 2009 10:19:25 +0530

aspell-or (0.03-4) unstable; urgency=low

  [Kartik Mistry]
  * debian/control:
    + Updated Standards-Version to 3.8.2
    + [Lintian] Added ${misc:Depends} for debhelper dependency
  * debian/control:
    + Updated to use correct copyright symbol ©
  * debian/rules:
    + Added clean rule target to remove Makefile from diff.gz

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Mon, 22 Jun 2009 14:02:58 +0530

aspell-or (0.03-3) unstable; urgency=low

  [Kartik Mistry]
  * Added debian/watch file
  * debian/copyright:
    + Updated copyright year
    + Moved copyright out of license section
    + Updated link of GPL
  * debian/control:
    + Updated short description
    + Added Homepage entry as control field
    + Updated Standards-Version to 3.7.3
    + Added VCS-* fields
  * debian/changelog:
    + Fixed typo of compatibility

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sat, 26 Jan 2008 14:47:52 +0530

aspell-or (0.03-2) unstable; urgency=low

  [Kartik Mistry]
  * Set maintainer address to Debian-IN Team
  * Updated standards-version to 3.7.2
  * Updated debhelper compatibility to 5
  * debian/copyright: updated according to standard coptright file
  * debian/control: fixed long descriptions, fixed build-dependency
  * debian/changelog: removed useless empty line at end

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Mon, 21 May 2007 11:27:13 +0530

aspell-or (0.03-1) unstable; urgency=low

  * Initial Release.  (Closes: #330359)

 -- Soumyadip Modak <soumyadip@softhome.net>  Fri, 23 Sep 2005 07:15:47 +0530
